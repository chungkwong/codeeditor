/*
 * Copyright (C) 2017 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.codeeditor;
import com.github.chungkwong.codeeditor.lex.*;
import com.github.chungkwong.codeeditor.ui.*;
import com.github.chungkwong.json.*;
import java.awt.*;
import java.io.*;
import javax.swing.*;
import javax.swing.text.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Main{
	public static void main(String[] args) throws Exception{
		JFrame f=new JFrame("Test");
		JEditorPane editor=new JEditorPane();
		editor.setEditorKit(new javax.swing.text.StyledEditorKit());
		editor.setEditable(true);
		new SyntaxHighlightSupport(getExampleLex(),getExampleScheme()).apply((StyledDocument)editor.getDocument());
		new AutoCompleteSupport(getHints()).apply(editor);
		f.add(new JScrollPane(editor),BorderLayout.CENTER);
		f.setExtendedState(JFrame.MAXIMIZED_BOTH);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
		editor.requestFocusInWindow();
	}
	private static StyleScheme getExampleScheme() throws IOException, SyntaxException{
		/*StyleScheme scheme=new StyleScheme("example");
		SimpleAttributeSet italic=new SimpleAttributeSet();
		StyleConstants.setItalic(italic,true);
		scheme.addStyle("NUM",italic);
		SimpleAttributeSet bold=new SimpleAttributeSet();
		StyleConstants.setBold(bold,true);
		scheme.addStyle("LETTER",bold);
		SimpleAttributeSet red=new SimpleAttributeSet();
		StyleConstants.setForeground(red,Color.RED);
		scheme.addStyle("OTHER",red);
		return scheme;*/
		return StyleScheme.fromJSON("{\"name\":\"example\",\"styles\":{\"NUM\":{\"italic\":true},\"LETTER\":{\"bold\":true},\"OTHER\":{\"foreground\":\"FFFF0000\"}}}");
	}
	private static Lex getExampleLex(){
		NaiveLex lex=new NaiveLex();
		lex.addType("NUM","[0-9]+");
		lex.addType("LETTER","[a-zA-Z]+");
		lex.addType("OTHER","[^0-9a-zA-Z]+");
		return lex;
	}
	private static HintProvider getHints(){
		return new HintProvider() {
			@Override
			public Hint[] getHints(Document doc,int pos){
				return new Hint[]{
					Hint.createHint("ls","ls",null,"list files"),
					Hint.createHint("lpr","lpr",null,"print"),
					Hint.createHint(Integer.toString(pos),Integer.toString(pos),null,"position")
				};
			}
		};
	}
}