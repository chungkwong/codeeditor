/*
 * Copyright (C) 2017 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.codeeditor.ui;
import com.github.chungkwong.codeeditor.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class AutoCompleteSupport implements CaretListener,AncestorListener{
	private final HintProvider hintProvider;
	private RealtimeTask hintDaemon;
	//private PopupHint popup;
	private final PopupHint popup=new PopupHint();
	public AutoCompleteSupport(HintProvider hintProvider){
		this.hintProvider=hintProvider;
	}
	public void apply(JTextComponent comp){
		comp.addCaretListener(this);
		comp.addAncestorListener(this);
	}
	public void updateHint(){

	}
	public void showHint(Hint[] hints,JTextComponent editor){
		if(hints.length>0){
			popup.hide();
			popup.show(hints,editor);
		}
	}
	@Override
	public void caretUpdate(CaretEvent e){
		hintDaemon.invoke();
	}
	@Override
	public synchronized void ancestorAdded(AncestorEvent event){
		if(hintDaemon==null){
			JTextComponent editor=(JTextComponent)event.getComponent();
			hintDaemon=new RealtimeTask(
					()->SwingUtilities.invokeLater(()->showHint(hintProvider.getHints(editor.getDocument(),editor.getCaretPosition()),editor)));
			hintDaemon.invoke();
		}
	}
	@Override
	public synchronized void ancestorRemoved(AncestorEvent event){
		hintDaemon.stop();
		hintDaemon=null;
	}
	@Override
	public void ancestorMoved(AncestorEvent event){

	}
}
