/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.chungkwong.codeeditor.ui;
import com.github.chungkwong.codeeditor.util.*;
import com.github.chungkwong.json.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.stream.*;
import javax.swing.text.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class StyleScheme{
	private final String name;
	private final HashMap<String,AttributeSet> style=new HashMap<>();
	public StyleScheme(String name){
		this.name=name;
	}
	public void addStyle(String type,AttributeSet set){
		style.put(type,set);
	}
	public AttributeSet getStyle(String type){
		return style.get(type);
	}
	public String toJSON(){
		Map<String,Object> obj=new HashMap<>();
		obj.put("name",name);
		obj.put("styles",style.entrySet().stream().collect(Collectors.toMap((e)->e.getKey(),(e)->attributeSetToJson(e.getValue()))));
		return JSONConvertor.toJSON(obj);
	}
	public static StyleScheme fromJSON(String json) throws IOException, SyntaxException{
		Map<String,Object> obj=(Map<String,Object>)JSONConvertor.fromJSON(json);
		StyleScheme scheme=new StyleScheme((String)obj.get("name"));
		((Map<String,Map<String,Object>>)obj.get("styles")).forEach((key,value)->scheme.addStyle(key,scheme.jsonToAttributeSet(value)));
		scheme.style.forEach((k,v)->{
			if(v.isDefined("parent"))
				((MutableAttributeSet)v).setResolveParent(scheme.style.get((String)v.getAttribute("parent")));
		});
		return scheme;
	}
	private AttributeSet jsonToAttributeSet(Map<String,Object> map){
		SimpleAttributeSet set=new SimpleAttributeSet();
		for(Map.Entry<String,Object> entry:map.entrySet()){
			String key=entry.getKey();
			Type type=TYPES.get(key);
			Object name=CONSTANTS.get(key);
			Object rawValue=entry.getValue();
			Object value;
			switch(type){
				case BOOLEAN:value=(Boolean)rawValue;break;
				case INT:value=((Number)rawValue).intValue();break;
				case FLOAT:value=((Number)rawValue).floatValue();break;
				case COLOR:value=decodeColor((String)rawValue);break;
				case STRING:value=(String)rawValue;break;
				case COMPONENT:value=null;/*FIXME*/break;
				case ICON:value=null;/*FIXME*/;break;
				case TAB_SET:value=null;/*FIXME*/;break;
				case PARENT:value=(String)rawValue;break;
				default:continue;
			}
			set.addAttribute(name,value);
		}
		return set;
	}
	private static String encodeColor(Color color){
		return Integer.toHexString(color.getRGB());
	}
	private static Color decodeColor(String text){
		return new Color(Integer.parseUnsignedInt(text,16),true);
	}
	private Map<String,Object> attributeSetToJson(AttributeSet set){
		Map<String,Object> map=new HashMap<>();
		Enumeration<?> iter=set.getAttributeNames();
		while(iter.hasMoreElements()){
			Object key=iter.nextElement();
			String name=CONSTANTS.getKey(key);
			if(name==null)
				continue;
			Type type=TYPES.get(name);
			Object value=set.getAttribute(key);
			Object rawValue;
			switch(type){
				case BOOLEAN:rawValue=(Boolean)value;break;
				case INT:rawValue=((Number)value).intValue();break;
				case FLOAT:rawValue=((Number)value).floatValue();break;
				case COLOR:rawValue=encodeColor((Color)value);break;
				case STRING:rawValue=(String)value;break;
				case COMPONENT:rawValue=null;/*FIXME*/break;
				case ICON:rawValue=null;/*FIXME*/;break;
				case TAB_SET:rawValue=null;/*FIXME*/;break;
				case PARENT:rawValue=(String)value;break;
				default:continue;
			}
			map.put(name,rawValue);
		}
		return map;
	}
	public static String toString(AttributeSet set){
		StringBuilder buf=new StringBuilder();
		Enumeration<?> names=set.getAttributeNames();
		while(names.hasMoreElements()){
			Object name=names.nextElement();
			buf.append(name.getClass()).append(':').append(name).append('=');
			Object value=set.getAttribute(name);
			buf.append(value.getClass()).append(':').append(value).append('\n');
		}
		return buf.toString();
	}
	private static final InvertibleMap<String,Object> CONSTANTS=new InvertibleMap<>();
	private static final Map<String,Type> TYPES=new HashMap<>();
	static{
		CONSTANTS.put("alignment",StyleConstants.Alignment);
		CONSTANTS.put("background",StyleConstants.Background);
		CONSTANTS.put("bidi",StyleConstants.BidiLevel);
		CONSTANTS.put("bold",StyleConstants.Bold);
		CONSTANTS.put("component",StyleConstants.ComponentAttribute);
		CONSTANTS.put("composed text",StyleConstants.ComposedTextAttribute);
		CONSTANTS.put("family",StyleConstants.Family);
		CONSTANTS.put("first line indent",StyleConstants.FirstLineIndent);
		CONSTANTS.put("font family",StyleConstants.FontFamily);
		CONSTANTS.put("font size",StyleConstants.FontSize);
		CONSTANTS.put("foreground",StyleConstants.Foreground);
		CONSTANTS.put("icon",StyleConstants.IconAttribute);
		CONSTANTS.put("italic",StyleConstants.Italic);
		CONSTANTS.put("left indent",StyleConstants.LeftIndent);
		CONSTANTS.put("line spacing",StyleConstants.LineSpacing);
		CONSTANTS.put("model",StyleConstants.ModelAttribute);
		CONSTANTS.put("name",StyleConstants.NameAttribute);
		CONSTANTS.put("orientation",StyleConstants.Orientation);
		CONSTANTS.put("resolve attribute",StyleConstants.ResolveAttribute);
		CONSTANTS.put("right indent",StyleConstants.RightIndent);
		CONSTANTS.put("size",StyleConstants.Size);
		CONSTANTS.put("space above",StyleConstants.SpaceAbove);
		CONSTANTS.put("space below",StyleConstants.SpaceBelow);
		CONSTANTS.put("strike through",StyleConstants.StrikeThrough);
		CONSTANTS.put("subscript",StyleConstants.Subscript);
		CONSTANTS.put("superscript",StyleConstants.Superscript);
		CONSTANTS.put("tab set",StyleConstants.TabSet);
		CONSTANTS.put("underline",StyleConstants.Underline);
		CONSTANTS.put("parent","parent");
		TYPES.put("alignment",Type.INT);
		TYPES.put("background",Type.COLOR);
		TYPES.put("bidi",Type.INT);
		TYPES.put("bold",Type.BOOLEAN);
		TYPES.put("component",Type.COMPONENT);
		//TYPES.put("composed text",Type.ComposedTextAttribute);
		TYPES.put("family",Type.STRING);
		TYPES.put("first line indent",Type.FLOAT);
		TYPES.put("font family",Type.STRING);
		TYPES.put("font size",Type.INT);
		TYPES.put("foreground",Type.COLOR);
		TYPES.put("icon",Type.ICON);
		TYPES.put("italic",Type.BOOLEAN);
		TYPES.put("left indent",Type.FLOAT);
		TYPES.put("line spacing",Type.FLOAT);
		//TYPES.put("model",Type.ModelAttribute);
		TYPES.put("name",Type.STRING);
		//TYPES.put("orientation",Type.Orientation);
		//TYPES.put("resolve attribute",Type.ResolveAttribute);
		TYPES.put("right indent",Type.FLOAT);
		TYPES.put("size",Type.INT);
		TYPES.put("space above",Type.FLOAT);
		TYPES.put("space below",Type.FLOAT);
		TYPES.put("strike through",Type.BOOLEAN);
		TYPES.put("subscript",Type.BOOLEAN);
		TYPES.put("superscript",Type.BOOLEAN);
		TYPES.put("tab set",Type.TAB_SET);
		TYPES.put("underline",Type.BOOLEAN);
		TYPES.put("parent",Type.PARENT);
	}
	private static enum Type{
		BOOLEAN,INT,FLOAT,COLOR,STRING,COMPONENT,ICON,TAB_SET,PARENT
	}
}
